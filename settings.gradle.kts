pluginManagement {
    repositories {
        google()
        gradlePluginPortal()
        mavenCentral()
    }
}

rootProject.name = "combined_kmm"
include(":androidSdk")
include(":shared")